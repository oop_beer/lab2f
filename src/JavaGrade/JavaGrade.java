package JavaGrade;

import java.util.Scanner;

public class JavaGrade {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input your score: ");
        int score = sc.nextInt();
        if (score>=80) {
            System.out.println("Grade = A");
        } else if (score<80 && score>=75){
            System.out.println("Grade = B+");
        } else if (score<75 && score>=70){
            System.out.println("Grade = B");
        } else if (score<70 && score>=65){
            System.out.println("Grade = C+");
        } else if (score<65 && score>=60){
            System.out.println("Grade = C");
        } else if (score<60 && score>=55){
            System.out.println("Grade = D+");
        } else if (score<55 && score>=50){
            System.out.println("Grade = D");
        } else if(score<50){
            System.out.println("Grade = F");
        }
    }
    }
    

